# guld-ledger-cli

Guld ledger command line interface.

### Install

```
npm i -g guld-ledger-cli
```

### Usage


##### CLI

```
  Usage: guld-ledger [options] [command]

  Guld ledger

  Options:

    -V, --version     output the version number
    -u --user <name>  The user name to run as.
    -h, --help        output usage information

  Commands:

    cache [path]      Cache all ledger files found (recursively) in the given path or $HOME/ledger.
```
